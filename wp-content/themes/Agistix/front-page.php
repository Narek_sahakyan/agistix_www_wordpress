<?php
/**
 * The main template file
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file
 *
 * Please see /external/bootstrap-utilities.php for info on BsWp::get_template_parts()
 *
 * @package 	WordPress
 * @subpackage 	Bootstrap 3.3.7
 * @autor 		Babobski
 */
?>
<?php BsWp::get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>

<!-- banner-section -->
<section class="banner-section section-lg home-banner" style="background-image: url(<?php echo get_template_directory_uri(); ?>/img/general/banner/home.jpg)">
    <div class="container">
        <h1 class="banner-title spec-title">Achieve total visibility into every transaction, every shipment.</h1>
    </div>

    <div id="sparkSVG" class="banner-svg-wrapper"></div>
    <p class="banner-desc top-desc" data-text=".top-desc">See real-time data for every inbound and
        <br>outbound transaction across the globe.</p>
    <p class="banner-desc right-desc" data-text=".right-desc">Easily spot transactions that
        <br>don’t meet your standards.</p>
    <p class="banner-desc left-desc" data-text=".left-desc">Quantify the impact of your work—
        <br>and easily share with stakeholders.</p>
</section>
<!-- end banner-section -->

<?php BsWp::get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer') ); ?>
