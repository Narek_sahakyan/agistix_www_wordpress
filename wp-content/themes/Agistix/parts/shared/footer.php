
<footer class="page-footer">
	<!-- page-footer -->
	<div class="container">
		<!-- begin footer-menu -->
		<?php
			wp_nav_menu( array(
				'menu'              => 'primary',
				'theme_location'    => 'primary',
				'depth'             => 2,
				'container'         => false,
				'menu_class'        => 'footer-menu',
				'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
				'walker'            => new wp_bootstrap_navwalker())
			);
		?>
		<!-- end footer-menu -->
	</div>
	<!-- end page-footer -->

	<!-- begin copyright -->
	<div class="copyright">
		<div class="container-fluid">
			<p>+1-888-244-7849 or <a href="mailto:info@agistix.com">info@agistix.com</a> 1900 Alameda de las Pulgas, Suite 100, San Mateo, CA 94403 USA</p>
			<p>&copy; 2017 Agistix Inc. All rights reserved. <a href="./privacy">Privacy Policy</a>
			</p>
		</div>
	</div>
	<!-- end copyright -->

</footer>