<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * Please see /external/bootsrap-utilities.php for info on BsWp::get_template_parts()
 *
 * @package 	WordPress
 * @subpackage 	Bootstrap 3.3.7
 * @autor 		Babobski
 */
?>
<?php BsWp::get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>


<div class="page-wrapper">
	<?php if (has_post_thumbnail( $post->ID ) ): ?>
		<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
		<section class="banner-section  about-us-banner" style="background-image: url('<?php echo $image[0]; ?>')">
	<?php else: ?>
		<section class="banner-section  about-us-banner" style="background-image: url(<?php echo get_template_directory_uri(); ?>/img/general/banner/home.jpg)">
	<?php endif; ?>
		<div class="container">
			<h1 class="banner-title spec-title"><?php the_title(); ?></h1>
		</div>
		<div class="banner-backdrop"></div>
	</section>
</div>

<?php BsWp::get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer' ) ); ?>
