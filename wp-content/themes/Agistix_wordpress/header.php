<!doctype html>

<!--[if IEMobile 7 ]> <html <?php language_attributes(); ?>class="no-js iem7"> <![endif]-->
<!--[if lt IE 7 ]> <html <?php language_attributes(); ?> class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html <?php language_attributes(); ?> class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html <?php language_attributes(); ?> class="no-js ie8"> <![endif]-->
<!--[if (gte IE 9)|(gt IEMobile 7)|!(IEMobile)|!(IE)]><!--><html <?php language_attributes(); ?> class="no-js"><!--<![endif]-->

	<head>
		<meta charset="<?php bloginfo('charset'); ?>">

  		<meta http-equiv="x-ua-compatible" content="ie=edge">
  		<META HTTP-EQUIV="Content-type" CONTENT="text/html; charset=<?php bloginfo('charset'); ?>">
  		<meta name="viewport" content="width=device-width, initial-scale=1">
  		<meta name="description" content="<?php bloginfo('description'); ?>">
  		<meta name="keywords" content="<?php bloginfo('description'); ?>">

  		<title><?php wp_title( '|', true, 'right' ); ?><?php bloginfo( 'name' ); ?></title>
  		<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
  		<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon/favicon.ico"/>

  		<meta property="og:title" content="<?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' :'; } ?> <?php bloginfo('name'); ?>" />
  		<meta property="og:type" content="website" />
  		<meta property="og:url" content="https://agistix.com" />
  		<meta property="og:description" content="<?php bloginfo('description'); ?>" />
  		<meta property="og:image" content="<?php echo get_template_directory_uri(); ?>images/general/banner/about.jpg" />
  		<meta property="og:image:secure_url" content="<?php echo get_template_directory_uri(); ?>images/general/banner/about.jpg" />
  		<meta property="og:image:type" content="image/jpeg" />
  		<meta property="og:image:width" content="1366" />
  		<meta property="og:image:height" content="341" />
  		<meta property="twitter:description" content="<?php wp_title( '|', true, 'right' ); ?><?php bloginfo( 'name' ); ?>" />
  		<link rel="image_src" href="<?php echo get_template_directory_uri(); ?>/images/general/banner/about.jpg" />

  		<link href="//www.google-analytics.com" rel="dns-prefetch">
  		<link href="<?php echo get_template_directory_uri(); ?>/favicon/favicon.ico" rel="shortcut icon">
  		<link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_template_directory_uri(); ?>/favicon/apple-touch-icon.png">
  		<link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/favicon/favicon-32x32.png" sizes="32x32">
  		<link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/favicon/favicon-16x16.png" sizes="16x16">
  		<link rel="manifest" href="<?php echo get_template_directory_uri(); ?>/favicon/manifest.json">
  		<link rel="mask-icon" href="<?php echo get_template_directory_uri(); ?>/favicon/safari-pinned-tab.svg" color="#5bbad5">
  		<meta name="apple-mobile-web-app-title" content="Agistix">
  		<meta name="application-name" content="Agistix">
  		<meta name="theme-color" content="#ffffff">

		<!-- wordpress head functions -->
		<?php wp_head(); ?>
		<!-- end of wordpress head -->
	</head>

	<body <?php body_class(); ?>>

		<header role="banner">

			<header class="header-navbar navbar-fixed-top navbar-default">
				<div class="container">

					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbarMain-collapse" aria-expanded="false">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar top"></span>
							<span class="icon-bar middle"></span>
							<span class="icon-bar bottom"></span>
						</button>

						<a class="navbar-brand" href="<?php echo home_url(); ?>" title="<?php echo get_bloginfo('description'); ?>">
							<img src="<?php echo get_template_directory_uri(); ?>/images/general/logo.svg" height="40" alt="Agistix">
						</a>
					</div>

					<div class="collapse navbar-collapse navbar-responsive-collapse">
						<?php wp_bootstrap_main_nav(); // Adjust using Menus in Wordpress Admin ?>
					</div>

				</div> <!-- end .container -->
			</div> <!-- end .navbar -->

		</header> <!-- end header -->

		<div class="container">
