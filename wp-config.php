<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'agistix');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'zt+~6j<|J6x5^{wj(xvx{V&$-/D9a#kp([;2C<1]^d50^rc{I6Y`3USZ0T5O&S c');
define('SECURE_AUTH_KEY',  'gzq@-0;|FEX>Mc<$9}rx3R_0[m.rmPrwS!52LP8%[BJS?xWG]1Dvpxb%Pg&B:^8]');
define('LOGGED_IN_KEY',    'IU=^wclOFas]:JmOJDY%2Q_Agh7gaQdcLsh*_*U^^9#i<N+D7aGjj,hHM&U@b&Fl');
define('NONCE_KEY',        '3#[nQ61-S%^NGvL7Vpez>/uc,d3lcD~3q^d]Ve0~c+Z~Ahlkez8~WLQ11D$OsFD(');
define('AUTH_SALT',        'rHR2A~  viGEwtOr5w6&%<K=$OPIz^B5~iDoC1<%%KSkmTc;)8q.PM:12-ZeH@5V');
define('SECURE_AUTH_SALT', '(EF[$I2Dr-iU>M`-Ey;LjHV?vAsjU?H!e5-TpTRS[n|_MO8rnI)`3`+5?u79Qd60');
define('LOGGED_IN_SALT',   'g o]VD-j,u9AZTu6H2-7ndM9LnT[YT73vr<3@0h%>G*E/_oKA`;cmy:dj2y{3<Y~');
define('NONCE_SALT',       'L1UrfpSL>:*C%Gg.2(S/h^ciGR(Si&lOIG^EC{$3<d5+EaxSz~D1lck,JIoC]1dV');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
